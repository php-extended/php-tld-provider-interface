<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tld-provider-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Tld;

use RuntimeException;
use Stringable;

/**
 * TopLevelDomainProviderInterface interface file.
 * 
 * This class represents a factory for a top level domain hierarchy.
 * 
 * @author Anastaszor
 */
interface TopLevelDomainProviderInterface extends Stringable
{
	
	/**
	 * Builds a new hierarchy from the source.
	 * 
	 * @return TopLevelDomainHierarchyInterface
	 * @throws RuntimeException if something goes wrong with the building process
	 */
	public function getHierarchy() : TopLevelDomainHierarchyInterface;
	
}
