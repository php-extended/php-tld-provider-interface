# php-extended/php-tld-provider-interface

A library to build tld hierarchies from external sources.

![coverage](https://gitlab.com/php-extended/php-tld-provider-interface/badges/master/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install php-extended/php-tld-provider-interface ^8`


## Basic Usage

This library is an interface-only library.

 For an example usage, see [`php-extended/php-tld-provider-mozilla`](https://gitlab.com/php-extended/php-tld-provider-mozilla).


## License

MIT (See [license file](LICENSE)).
